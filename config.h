#define CMDLENGTH 200
#define DELIMITER " "
#define CLICKABLE_BLOCKS

const Block blocks[] = {
//	BLOCK("sb-mail",    1800, 17),
//	BLOCK("sb-music",   0,    18),
//	BLOCK("sb-disk",    1800, 19),
//	BLOCK("sb-memory",  10,   20),
//	BLOCK("sb-loadavg", 5,    21),
//	BLOCK("sb-mic",     0,    26),
//	BLOCK("sb-record",  0,    27),
//	BLOCK("sb-volume",  0,    22),
//	BLOCK("sb-battery", 5,    23),
//	BLOCK("sb-date",    1,    24)
	BLOCK("sb-updates", 0,    17),
	BLOCK("sb-battery", 5,    18),
	BLOCK("sb-brightness", 5, 19),
	BLOCK("sb-cpu",     5,    20),
	BLOCK("sb-mem",     10,   21),
	BLOCK("sb-wlan",    5,    22),
	BLOCK("date",   1,    23)
	//BLOCK("echo '^b#aaaaaa^ ^c#000000^1'",   1,    23)
};
